# -*- org-confirm-babel-evaluate: nil; -*-
#+AUTHOR: Dan Čermák
#+DATE: August 6, 2022
#+EMAIL: defolos@fedoraproject.org
#+TITLE: Running Home Assistant on Fedora IoT


#+REVEAL_ROOT: ./node_modules/reveal.js/
#+REVEAL_THEME: simple
#+REVEAL_PLUGINS: (highlight notes history)
#+OPTIONS: toc:nil
#+REVEAL_DEFAULT_FRAG_STYLE: appear
#+REVEAL_INIT_OPTIONS: transition: 'none', hash: true
#+OPTIONS: num:nil toc:nil center:nil reveal_title_slide:nil
#+REVEAL_EXTRA_CSS: ./node_modules/@fortawesome/fontawesome-free/css/all.min.css
#+REVEAL_EXTRA_CSS: ./custom-style.css
#+REVEAL_HIGHLIGHT_CSS: ./node_modules/reveal.js/plugin/highlight/zenburn.css

#+REVEAL_TITLE_SLIDE: <h2 class="title"><img src="./media/ha_logo.svg" height="100px" style="margin-bottom:-22px"/> %t</h2>
#+REVEAL_TITLE_SLIDE: <p class="subtitle" style="color: Gray;">%s</p>
#+REVEAL_TITLE_SLIDE: <p class="author">%a</p>
# #+REVEAL_TITLE_SLIDE: <p class="date">%d</p>
#+REVEAL_TITLE_SLIDE: <div style="float:left"><a href="https://flocktofedora.org/"><img src="./media/nest_logo.svg" height="50px" style="margin-bottom:-5px"/> 2022</a></div>
#+REVEAL_TITLE_SLIDE: <div style="float:right;font-size:35px;"><p xmlns:dct="http://purl.org/dc/terms/" xmlns:cc="http://creativecommons.org/ns#"><a href="https://creativecommons.org/licenses/by/4.0" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">
#+REVEAL_TITLE_SLIDE: CC BY 4.0 <i class="fab fa-creative-commons"></i> <i class="fab fa-creative-commons-by"></i></a></p></div>


* who -u

Dan Čermák

@@html: <div style="float:center">@@
@@html: <table class="who-table">@@
@@html: <tr><td><i class="fab fa-suse"></i></td><td> Software Developer @SUSE</td></tr>@@
@@html: <tr><td><i class="fab fa-fedora"></i></td><td> i3 SIG, Package maintainer</td></tr>@@
@@html: <tr><td><i class="far fa-heart"></i></td><td> Developer Tools, Testing and Documentation, Home Automation</td></tr>@@
@@html: <tr></tr>@@
@@html: <tr></tr>@@
@@html: <tr><td><i class="fa-solid fa-globe"></i></td><td> <a href="https://dancermak.name/">https://dancermak.name</a></td></tr>@@
@@html: <tr><td><i class="fab fa-github"></i></td><td> <a href="https://github.com/dcermak/">dcermak</a> / <a href="https://github.com/D4N/">D4N</a></td></tr>@@
@@html: <tr><td><i class="fab fa-twitter"></i></td><td> <a href="https://twitter.com/DefolosDC/">@DefolosDC</a></td></tr>@@
@@html: <tr><td><i class="fab fa-mastodon"></i></td><td> <a href="https://mastodon.social/@Defolos">@Defolos@mastodon.social</a></td></tr>@@
@@html: </table>@@
@@html: </div>@@


* Agenda

  - [[What is Home Assistant][What is Home Assistant]]
  - [[Fedora IoT][Running HA on Fedora IoT]]
  - [[Tweaking the setup][Tweaking the setup]]
  - [[Questions?][Questions?]]

* What is Home Assistant

#+begin_notes
#+end_notes

#+ATTR_REVEAL: :frag (appear)
- FLOSS home automation
- integration with nearly *every* smart home device
- intuitive & simple UI
- powerful templating, blueprint & scene system
- companion mobile App

** Sales Pitch

#+begin_notes
#+end_notes

Why should I use Home Assistant?
#+ATTR_REVEAL: :frag (appear)
- cloud free/self-hosted home automation
- runs on *your* machine(s)
- automate *everything* to your heart's desire
- support for energy tracking


** Where to run Home Assistant?

#+ATTR_REVEAL: :frag (appear appear) :frag_idx (1 2)
- for testing: (spare) PC
- for production: low power-usage machine

#+ATTR_REVEAL: :frag appear :frag_idx 3
Do I need a Pi 4 for HA?

#+ATTR_REVEAL: :frag appear :frag_idx 4
*No*


* Fedora IoT

@@html: <img src="./media/fedora-iot.svg" height="200px"/>@@

#+ATTR_REVEAL: :frag (appear)
- rpm-ostree based system
- provisioned via ignition & Zezere
- ready for deploying containerized applications

** Required Hardware

#+ATTR_REVEAL: :frag (appear)
1. PC (with Fedora)
2. SD-Card & SD-Card reader
3. Raspberry Pi 3/4

** Getting started

#+begin_notes
#+end_notes

#+ATTR_REVEAL: :frag (appear appear appear) :frag_idx (1 2 3)
- grab the image & =-CHECKSUM= from [[https://getfedora.org/en/iot/download/][getfedora.org]]
- ~dnf install gnupg2 arm-image-installer~
- verify the image:
#+ATTR_REVEAL: :frag appear :frag_idx 3
#+begin_src bash
curl -O https://getfedora.org/static/fedora.gpg
gpgv --keyring ./fedora.gpg *-CHECKSUM
sha256sum -c *-CHECKSUM
#+end_src

#+ATTR_REVEAL: :frag (appear appera) :frag_idx (4 5)
- find the SD-Card =/dev/= entry via ~udiskctl~, ~lsblk~ or ~fdisk -l~
- flash the image:
#+ATTR_REVEAL: :frag appear :frag_idx 5
#+begin_src bash
$ arm-image-installer --image=Fedora-IoT-[version].raw.xz \
      --target=rpi4 --media=/dev/XXX \
      --addkey=/path/to/pubkey \
      --resizefs
#+end_src

** Deploy with Zezere

#+ATTR_REVEAL: :frag (appear)
1. go to [[https://provision.fedoraproject.org/][provision.fedoraproject.org]]
2. claim unowned device \rightarrow claim \rightarrow submit provisioning request
3. copy ssh keys


** Run Home Assistant

#+begin_src bash
$ mkdir /path/to/conf/dir
$ podman run -d -v /path/to/conf/dir:/config:Z \
      -v /etc/localtime:/etc/localtime:ro \
      --privileged --network=host \
      --name=homeassistant \
      ghcr.io/home-assistant/home-assistant:stable
#+end_src

#+ATTR_REVEAL: :frag (appear) :frag_idx (2)
Visit http://rpi_ip:8123 and follow [[https://www.home-assistant.io/getting-started/onboarding/][onboarding]]

#+REVEAL: split
*THE END*

* Tweaking the setup

#+ATTR_REVEAL: :frag (appear)
1. run via systemd
2. setup https
3. ZigBee


** Run via systemd

#+begin_src bash
$ podman generate systemd --new homeassistant > \
    /etc/systemd/system/homeassistant.service
$ podman stop homeassistant
$ systemctl daemon-reload
$ systemctl enable --now homeassistant
#+end_src

** Container auto-updates

#+ATTR_REVEAL: :frag appear :frag_idx 1
- Add the flag:
#+ATTR_REVEAL: :frag appear :frag_idx 1
#+begin_src bash
--label "io.containers.autoupdate=registry"
#+end_src

#+ATTR_REVEAL: :frag appear :frag_idx 2
- Manual update via:
#+ATTR_REVEAL: :frag appear :frag_idx 2
#+begin_src bash
podman auto-update
#+end_src

#+ATTR_REVEAL: :frag appear :frag_idx 3
- Scheduled update:
#+ATTR_REVEAL: :frag appear :frag_idx 3
#+begin_src bash
systemctl enable --now podman-auto-update.timer
#+end_src

#+ATTR_REVEAL: :frag appear :frag_idx 4
- More details in the [[https://fedoramagazine.org/auto-updating-podman-containers-with-systemd/][Fedora Magazine]]

** HTTPS

#+ATTR_REVEAL: :frag appear :frag_idx 1
- certificates in [[https://www.redhat.com/sysadmin/new-podman-secrets-command][podman secrets]]:
#+ATTR_REVEAL: :frag appear :frag_idx 1
#+begin_src bash
$ podman secret create fullchain_pem /path/to/fullchain.pem
$ podman secret create privkey_pem /path/to/privkey.pem
#+end_src

#+ATTR_REVEAL: :frag appear :frag_idx 2
- pass them to the container:
#+ATTR_REVEAL: :frag appear :frag_idx 2
#+begin_src bash
--secret fullchain_pem --secret privkey_pem
#+end_src

#+ATTR_REVEAL: :frag appear :frag_idx 3
- add to =configuration.yaml=:
#+ATTR_REVEAL: :frag appear :frag_idx 3
#+begin_src yaml
http:
  ssl_certificate: /run/secrets/fullchain_pem
  ssl_key: /run/secrets/privkey_pem
  server_port: 443
#+end_src

** ZigBee

- [[https://sonoff.tech/product/diy-smart-switch/sonoff-zigbee-3-0-usb-dongle-plus-e/][Zigbee 3.0 USB Dongle plus]] just works\trade


** VPN

- wireguard
- [[https://tailscale.com/][tailscale]]
- [[https://www.defined.net/nebula/][Nebula]]

*** Wireguard

- ~rpm-ostree install wireguard-tools~
#+begin_src bash
cd /etc/wireguard
umask 077
wg genkey | tee privatekey | wg pubkey > publickey
#+end_src

*** server config

=/etc/wireguard/wg0.conf=:
#+begin_src ini
[Interface]
Address = 10.200.200.1/24
ListenPort = 51820
PrivateKey = # insert private key here
# optional
PostUp = iptables -A FORWARD -i %i -j ACCEPT; iptables -A FORWARD -o %i -j ACCEPT; iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -D FORWARD -o %i -j ACCEPT; iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE

[Peer]
PublicKey = # insert pubkey of peer
AllowedIPs = 10.200.200.N/32
#+end_src

*** host config

=/etc/wireguard/wg0.conf=:
#+begin_src ini
[Interface]
PrivateKey = # insert private key here
Address = 10.200.200.N/24
PostUp = iptables -A FORWARD -i wg0 -j ACCEPT; iptables -t nat -A POSTROUTING -o wlp59s0 -j MASQUERADE
PostDown = iptables -D FORWARD -i wg0 -j ACCEPT; iptables -t nat -D POSTROUTING -o wlp59s0 -j MASQUERADE
ListenPort = 51820

[Peer]
PublicKey = # pubkey of server
Endpoint = # IP of the server
AllowedIPs = 10.200.200.0/24
PersistentKeepalive = 25
#+end_src

*** launch wireguard
~systemctl enable --now wg-quick@wg0~

* Get in Touch

- [[https://matrix.to/#/#iot:fedoraproject.org][#iot on chat.fedoraproject.org]]
- [[https://community.home-assistant.io/][Home Assistant Forum]]
- [[https://discord.gg/c5DvZ4e][Home Assistant Discord Server]]
- [[https://web.libera.chat/#wireguard][#wireguard on libera.chat]]


* Links

- [[https://www.home-assistant.io/getting-started/onboarding/][Home Assistant home page]]
- [[https://coreos.github.io/ignition/][Ignition]] and [[https://coreos.github.io/butane/][Butane]] for provisioning
- [[https://github.com/fedora-iot/zezere][Zezere]]
- [[https://coreos.github.io/zincati/][Zincati]] for =rpm-ostree= auto-updates
- slides: @@html: <i class="fab fa-gitlab"></i>@@ [[https://gitlab.com/D4N/nest2022][=D4N/nest2022=]]


* Questions?

#+ATTR_REVEAL: :frag appear :frag_idx 1
*Answers!*

* Legal

- MIT: [[https://revealjs.com/][reveal.js]]
- CC-BY-4.0 and SIL OFL 1.1 and MIT: [[https://fontawesome.com/][Font Awesome]]
- CC-BY-SA-NC-4.0 [[https://raw.githubusercontent.com/home-assistant/assets/master/logo/logo-pretty.svg][Home Assistant Logo]]
- IoT Logo & Nest Logo 
